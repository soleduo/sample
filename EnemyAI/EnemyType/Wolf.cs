using System.Collections;
using UnityEngine;
using Assets.Code.Interfaces;
using Assets.Code.States;
using Assets.Code.Scripts;
using Assets.Code.Characters;
using Assets.Code.Parts;

namespace Assets.Code.Characters
{
public class Wolf : Enemy {

		void Awake()
		{
			if (GetInstanceRef() == null)
				SetInstanceRef (this);
		}
	// Use this for initialization
	void Start () {
			SetMoveSpeed (4.2f);
			SetAttackRange (6f);
			int moveTime = Random.Range (3, 6); 
			SetMoveTime (moveTime);
			SetAttackCooldown (4.2f);
			SetAttackRate (0.8f);
			SetAgent(GetComponent<NavMeshAgent> ());
		}
	
	// Update is called once per frame
		public new void CharacterUpdate() {
			base.CharacterUpdate ();
		}


		


	public override Vector3 AttackInit(){
			var agent = GetAgent ();

			var targetObject = GetTargetObject ();
			Vector3 atkDir = GetTargetPosition() + gameObject.transform.TransformDirection(0,0,8);
			agent.Stop();

			return atkDir;
	}

	public override void AttackTimer(Vector3 target)
		{
			var agent = GetAgent ();
			var atkDir = target;
			var atkRate = GetAttackRate(); 
			float timer = TimerCount ();
			if ((timer) >= atkRate){
				DoAttack (atkDir, 8f);
			} 
			else {
				agent.Stop();
			}
		}


		public void DoAttack(Vector3 target, float speed){
			var agent = GetAgent ();
			agent.speed = speed;
			agent.acceleration = speed;
			agent.SetDestination(target);

			if (GetAttackHitBoolean () == true) {
				GetPlayerScript().OnHit();
			}

			if(agent.remainingDistance <= 1){
				agent.speed = 4.2f;
				SwitchState(new EnemyIdle(this));
				SetTimer(0);
				base.DoAttack();
			}


		}
		public override void OnDeath(){
			base.OnDeath ();
			SetPartsRef (new WolfParts (GetPartsManager()));
		}
				
	}
}