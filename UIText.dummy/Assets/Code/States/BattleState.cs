﻿using UnityEngine;
using System.Collections;
using Assets.Code.Scripts;
using Assets.Code.Buttons;

public class BattleState : MonoBehaviour {

	GameObject buttonManager;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {

			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if(GameObject.FindObjectOfType<ButtonManager>()!=null)
				buttonManager = GameObject.FindObjectOfType<ButtonManager>().gameObject;

			if(Physics.Raycast(ray, out hit, 100f)){
				if(buttonManager == null){
					if(hit.collider.CompareTag("Character"))
						hit.collider.GetComponent<Character>().OnClick();
				}
				else{
					if(hit.collider.CompareTag("Button")){
						hit.collider.GetComponent<Button>().OnClick();
						Debug.Log(hit.collider + " hit. Button Manager is " + buttonManager);
					}
					else{
								Destroy(buttonManager);
					}
				}
			}

		}
				
	}
}
