﻿using UnityEngine;
using System.Collections;
using Assets.Code.Interfaces;
using Assets.Code.States;

namespace Assets.Code.Scripts
{
	public class EventManager : MonoBehaviour {

		IStateBase activeState;
		public string fileDirectory;

		static EventManager instanceRef;

		void Awake()
		{
			if (instanceRef == null) {
				instanceRef = this;
			}

		}
	// Use this for initialization
		void Start () {
	
		}
	
	// Update is called once per frame
		void Update () {
			Debug.Log ("Press space to start");
			if(Input.GetKeyDown(KeyCode.Space)){
				activeState = new TextState(this, fileDirectory);
			}


			if (activeState != null) {
				activeState.StateUpdate();
			}
		}

		void OnGUI(){
			if (activeState != null) {
				activeState.ShowIt ();
			}

		}

		public void StartRoutine(IEnumerator typer){
			StartCoroutine (typer);

		}

		public void StopRoutine(string typer){
			StopRoutine (typer);
		}

		public void EndText(){
			activeState = null;
		}

		public void AddItem(){
			//AddItem method
		}

		public void SetFileDialog(string fileDialog){
			fileDirectory = fileDialog;
		}
		
	}
}