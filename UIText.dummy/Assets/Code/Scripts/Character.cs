﻿using UnityEngine;
using Assets.Code.Interfaces;
using System.Collections;
using Assets.Code.Scripts;

public class Character : MonoBehaviour {

	public GameObject button;
	bool button_ = false;
	GameObject chara;
	Vector3 screenPos;
	string[] buttons ;
	public string fileDir;

	// Use this for initialization
	void Start () {
		buttons = System.IO.File.ReadAllLines (fileDir);
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnGUI(){
		if (button_ == true && chara != null) {
			screenPos = Camera.main.WorldToScreenPoint (chara.transform.position);
			GameObject buttonTemp = Instantiate(button, chara.transform.position, chara.transform.rotation) as GameObject;
			buttonTemp.GetComponent<ButtonManager>().SetButton(buttons);
			button_ = false;
		}

	}
	public void OnClick(){
		if(GameObject.FindObjectOfType<ButtonManager>() == null)
		button_ = true;
		chara = collider.gameObject;
		Debug.Log(chara + " clicked.");
	}
}
