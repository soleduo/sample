﻿using UnityEngine;
using System.Collections;
using Assets.Code.Interfaces;
using Assets.Code.States;

namespace Assets.Code.Scripts
{
	public class GameController : MonoBehaviour {

		IStateBase activeState;

		private static GameController instanceRef;

		void Awake(){
			if (instanceRef == null) {
				instanceRef = this;
				DontDestroyOnLoad(gameObject);
				}
		}

	// Use this for initialization
		void Start () {
			if (activeState == null)
				activeState = new TitleScreen (this);
		}
	
	// Update is called once per frame
		void Update () {
			Debug.Log ("Active state is: " + activeState);
			if (activeState != null)
				activeState.StateUpdate ();
		}

		void OnGUI(){
			if (activeState != null)
				activeState.ShowIt ();
		}

		public void SwitchState(IStateBase newState){
			activeState = newState;
		}
	}
}