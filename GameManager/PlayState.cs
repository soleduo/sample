//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.Collections;
using UnityEngine;
using Assets.Code.Interfaces;
using Assets.Code.Scripts;
using Assets.Code.Characters;

namespace Assets.Code.States
{
	public class PlayState : IStateBase
	{
		GameController manager;	
		GameObject[] charactersRef;
		GameObject player;
			
		public PlayState (GameController managerRef)
		{
					manager = managerRef;
		}

		public void StateUpdate()
		{
			if (Application.loadedLevelName != "playScene") {
								Application.LoadLevel ("playScene");
				} else {
								Debug.Log ("This is PlayState StateUpdate");
								if (Input.GetKeyDown (KeyCode.Escape)) {
										Time.timeScale = 0;
								} else {
										player = GameObject.FindGameObjectWithTag ("Player");
										player.GetComponent<Player> ().CharacterUpdate ();

										charactersRef = GameObject.FindGameObjectsWithTag ("Enemy");
										if(charactersRef!=null){
										foreach (GameObject chars in charactersRef) {
												chars.GetComponent<Character> ().CharacterUpdate ();
												Debug.Log (chars.GetComponent<Character> () + " ");
										}}
								}
				}
		}


		public void ShowIt()
		{

		}

	}
}

